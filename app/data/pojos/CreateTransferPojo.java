package data.pojos;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Request body for creating money transfer
 */
public class CreateTransferPojo {
    @JsonProperty("account_from")
    private String accountFrom;
    @JsonProperty("account_to")
    private String accountTo;
    private double sum;
    private String currency;

    public String getAccountFrom() {
        return accountFrom;
    }

    public void setAccountFrom(final String accountFrom) {
        this.accountFrom = accountFrom;
    }

    public String getAccountTo() {
        return accountTo;
    }

    public void setAccountTo(final String accountTo) {
        this.accountTo = accountTo;
    }

    public double getSum() {
        return sum;
    }

    public void setSum(final double sum) {
        this.sum = sum;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(final String currency) {
        this.currency = currency;
    }
}
