package data.pojos;

import com.fasterxml.jackson.annotation.JsonProperty;
import data.enums.TransferStatus;

/**
 * Detail of money transfer
 */
public class TransferPojo {
    @JsonProperty("account_from")
    private String accountFrom;
    @JsonProperty("account_to")
    private String accountTo;
    private double sum;
    private String currency;
    private TransferStatus status;
    @JsonProperty("create_date")
    private Long createDate;
    @JsonProperty("process_date")
    private Long processDate;
    private String reservation;
    @JsonProperty("retry_count")
    private int retryCount;

    public String getAccountFrom() {
        return accountFrom;
    }

    public void setAccountFrom(final String accountFrom) {
        this.accountFrom = accountFrom;
    }

    public String getAccountTo() {
        return accountTo;
    }

    public void setAccountTo(final String accountTo) {
        this.accountTo = accountTo;
    }

    public double getSum() {
        return sum;
    }

    public void setSum(final double sum) {
        this.sum = sum;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(final String currency) {
        this.currency = currency;
    }

    public TransferStatus getStatus() {
        return status;
    }

    public void setStatus(final TransferStatus status) {
        this.status = status;
    }

    public Long getCreateDate() {
        return createDate;
    }

    public void setCreateDate(final Long createDate) {
        this.createDate = createDate;
    }

    public Long getProcessDate() {
        return processDate;
    }

    public void setProcessDate(final Long processDate) {
        this.processDate = processDate;
    }

    public String getReservation() {
        return reservation;
    }

    public void setReservation(final String reservation) {
        this.reservation = reservation;
    }

    public int getRetryCount() {
        return retryCount;
    }

    public void setRetryCount(final int retryCount) {
        this.retryCount = retryCount;
    }
}
