package data.pojos;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Pojo for account details transfer
 */
public class AccountPojo {
    private String uuid;
    private String currency;
    private double balance;
    @JsonProperty("debit_enabled")
    private boolean debitEnabled;
    @JsonProperty("disposable_balance")
    private double disposableBalance;

    public String getUuid() {
        return uuid;
    }

    public void setUuid(final String uuid) {
        this.uuid = uuid;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(final String currency) {
        this.currency = currency;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(final double balance) {
        this.balance = balance;
    }

    public boolean isDebitEnabled() {
        return debitEnabled;
    }

    public void setDebitEnabled(final boolean debitEnabled) {
        this.debitEnabled = debitEnabled;
    }

    public double getDisposableBalance() {
        return disposableBalance;
    }

    public void setDisposableBalance(final double disposableBalance) {
        this.disposableBalance = disposableBalance;
    }
}
