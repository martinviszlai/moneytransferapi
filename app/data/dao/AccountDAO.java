package data.dao;

import data.models.Account;
import exceptions.WrongParamException;
import play.db.jpa.JPAApi;

import javax.inject.Inject;
import java.util.List;

/**
 * Dao for DB operations regarding account
 */
public class AccountDAO {

    private final JPAApi jpaApi;

    @Inject
    public AccountDAO(final JPAApi jpaApi) {
        this.jpaApi = jpaApi;
    }

    /**
     * Save account into DB
     *
     * @param account account to be persisted
     * @return persisted account
     */
    public Account persistAccount(final Account account) {
        jpaApi.em().persist(account);
        return account;
    }

    /**
     * Get listing of all account identifiers
     *
     * @return list of uuid of accounts
     */
    public List<String> getAllAccounts() {
        final List<String> accounts = jpaApi.em().createQuery("SELECT acc.uuid FROM Account acc ORDER BY acc.uuid", String.class).getResultList();
        return accounts;
    }

    /**
     * Retrieve account details by provided uuid
     *
     * @param accountUuid identifier of account
     * @return all details of account with specified uuid
     */
    public Account getAccount(final String accountUuid) {
        if (accountUuid == null || accountUuid.isEmpty()) {
            throw new WrongParamException("Invalid account uuid!");
        }
        final Account account = jpaApi.em().find(Account.class, accountUuid);
        if (account == null) {
            throw new WrongParamException("Invalid account uuid!");
        }
        return account;
    }
}
