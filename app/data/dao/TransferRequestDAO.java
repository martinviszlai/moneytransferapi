package data.dao;

import data.enums.TransferStatus;
import data.models.TransferRequest;
import play.db.jpa.JPAApi;

import javax.inject.Inject;
import java.util.List;

/**
 * Dao for DB operations regarding transfers
 */
public class TransferRequestDAO {

    private final JPAApi jpaApi;

    @Inject
    public TransferRequestDAO(final JPAApi jpaApi) {
        this.jpaApi = jpaApi;
    }

    /**
     * Save request into DB
     *
     * @param transferRequest transferRequest to be persisted
     * @return persisted transferRequest
     */
    public TransferRequest persistTransferRequest(final TransferRequest transferRequest) {
        jpaApi.em().persist(transferRequest);
        return transferRequest;
    }

    /**
     * Load transfer request by uuid
     *
     * @param uuid identifier of transfer to be loaded
     * @return transferRequest
     */
    public TransferRequest getTransferRequest(final String uuid) {
        return jpaApi.em().find(TransferRequest.class, uuid);
    }

    /**
     * Get listing of all transfers for account filtered by status
     *
     * @param accountUuid uuid of account which transfers should be searched
     * @param status      status of transfers that should be filtered
     * @return listing of transfers
     */
    public List<TransferRequest> getTransferRequests(final String accountUuid, final TransferStatus status) {
        final StringBuilder queryString = new StringBuilder("SELECT req FROM TransferRequest req " +
            "WHERE (req.accountFrom.uuid LIKE :accountUuid OR req.accountTo.uuid LIKE :accountUuid)");
        if (status != null) {
            queryString.append(" AND req.status = " + status.ordinal());
        }

        final List<TransferRequest> requests = jpaApi.em().createQuery(queryString.toString(), TransferRequest.class).setParameter("accountUuid", accountUuid).getResultList();
        return requests;
    }

    /**
     * Get uuid of all unprocessed transfer requests
     *
     * @return listing of uuid of unprocessed transfers
     */
    public List<String> getUnprocessedTransferRequests() {
        final String queryString = "SELECT req.uuid FROM TransferRequest req " +
            "WHERE req.status = " + TransferStatus.OPEN.ordinal() + " OR req.status = " + TransferStatus.UNPROCESSED.ordinal();

        final List<String> requests = jpaApi.em().createQuery(queryString, String.class).getResultList();
        return requests;
    }
}