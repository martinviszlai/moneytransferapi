package data.dao;

import data.models.Reservation;
import play.db.jpa.JPAApi;

import javax.inject.Inject;

/**
 * Dao for DB operations regarding reservation
 */
public class ReservationDAO {

    private final JPAApi jpaApi;

    @Inject
    public ReservationDAO(final JPAApi jpaApi) {
        this.jpaApi = jpaApi;
    }

    /**
     * Save request into DB
     *
     * @param reservation reservation to be persisted
     * @return persisted reservation
     */
    public Reservation persistReservation(final Reservation reservation) {
        jpaApi.em().persist(reservation);
        return reservation;
    }

    /**
     * Delete request from DB
     *
     * @param reservation reservation to be deleted
     */
    public void deleteReservation(final Reservation reservation) {
        jpaApi.em().remove(reservation);
    }
}