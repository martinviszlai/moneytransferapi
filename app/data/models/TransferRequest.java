package data.models;

import data.enums.TransferStatus;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;

/**
 * Representation of transfer request
 */
@Entity
@Table(name = "transfer_request")
public class TransferRequest {

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "utils.UUIDGenerator")
    private String uuid;

    @ManyToOne
    @JoinColumn(name = "account_from_uuid")
    private Account accountFrom;

    @ManyToOne
    @JoinColumn(name = "account_to_uuid")
    private Account accountTo;

    private double sum;

    private String currency;

    @Column(name = "retry_count")
    private int retryCount;

    @Enumerated(EnumType.ORDINAL)
    private TransferStatus status;

    @Column(name = "create_date")
    @Temporal(value = TemporalType.TIMESTAMP)
    private Date createDate;

    @Column(name = "process_date")
    @Temporal(value = TemporalType.TIMESTAMP)
    private Date processDate;

    @OneToOne
    @JoinColumn(name = "reservation_uuid")
    private Reservation reservation;

    public String getUuid() {
        return uuid;
    }

    public void setUuid(final String uuid) {
        this.uuid = uuid;
    }

    public Account getAccountFrom() {
        return accountFrom;
    }

    public void setAccountFrom(final Account accountFrom) {
        this.accountFrom = accountFrom;
    }

    public Account getAccountTo() {
        return accountTo;
    }

    public void setAccountTo(final Account accountTo) {
        this.accountTo = accountTo;
    }

    public double getSum() {
        return sum;
    }

    public void setSum(final double sum) {
        this.sum = sum;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(final String currency) {
        this.currency = currency;
    }

    public int getRetryCount() {
        return retryCount;
    }

    public void setRetryCount(final int retryCount) {
        this.retryCount = retryCount;
    }

    public TransferStatus getStatus() {
        return status;
    }

    public void setStatus(final TransferStatus status) {
        this.status = status;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(final Date createDate) {
        this.createDate = createDate;
    }

    public Date getProcessDate() {
        return processDate;
    }

    public void setProcessDate(final Date processDate) {
        this.processDate = processDate;
    }

    public Reservation getReservation() {
        return reservation;
    }

    public void setReservation(final Reservation reservation) {
        this.reservation = reservation;
    }
}
