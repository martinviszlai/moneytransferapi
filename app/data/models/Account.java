package data.models;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Entity representing account
 */
@Entity
@Table(name = "account")
public class Account {

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "utils.UUIDGenerator")
    private String uuid;
    private String currency;
    private double balance;
    @Column(name = "debit_enabled", nullable = false)
    private boolean debitEnabled;

    @OneToMany(mappedBy = "account")
    private List<Reservation> reservations = new ArrayList<>();

    public String getUuid() {
        return uuid;
    }

    public void setUuid(final String uuid) {
        this.uuid = uuid;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(final String currency) {
        this.currency = currency;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(final double balance) {
        this.balance = balance;
    }

    public boolean isDebitEnabled() {
        return debitEnabled;
    }

    public void setDebitEnabled(final boolean debitEnabled) {
        this.debitEnabled = debitEnabled;
    }

    public List<Reservation> getReservations() {
        return reservations;
    }

    public void setReservations(final List<Reservation> reservations) {
        this.reservations = reservations;
    }
}
