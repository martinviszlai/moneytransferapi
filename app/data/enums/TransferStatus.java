package data.enums;

/**
 * Possible statuses of transfer request
 */
public enum TransferStatus {
    OPEN, PROCESSED, UNPROCESSED, FAILED
}
