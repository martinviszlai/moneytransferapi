package data.assemblers;

import data.models.TransferRequest;
import data.pojos.TransferPojo;
import exceptions.WrongParamException;

/**
 * Assembler for converting transfers from entity to pojos and vice versa
 */
public class TransferRequestAssembler {

    public TransferPojo assemblyPojo(final TransferRequest entity) {
        if (entity == null) {
            throw new WrongParamException("Corrupted entity object");
        }
        final TransferPojo pojo = new TransferPojo();
        pojo.setAccountFrom(entity.getAccountFrom().getUuid());
        pojo.setAccountTo(entity.getAccountTo().getUuid());
        pojo.setSum(entity.getSum());
        pojo.setCurrency(entity.getCurrency());
        pojo.setCreateDate(entity.getCreateDate().getTime());
        pojo.setStatus(entity.getStatus());
        pojo.setRetryCount(entity.getRetryCount());
        if (entity.getReservation() != null) {
            pojo.setReservation(entity.getReservation().getUuid());
        }
        if (entity.getProcessDate() != null) {
            pojo.setProcessDate(entity.getProcessDate().getTime());
        }
        return pojo;
    }
}
