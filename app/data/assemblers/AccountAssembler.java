package data.assemblers;

import data.models.Account;
import data.models.Reservation;
import data.pojos.AccountPojo;
import exceptions.WrongParamException;

/**
 * Assembler for converting accounts from entity to pojos and vice versa
 */
public class AccountAssembler {

    /**
     * Create new entity from account pojo
     *
     * @param pojo account with filled data
     * @return new entity with all data from entity
     */
    public Account assemblyEntity(final AccountPojo pojo) {
        if (pojo == null) {
            throw new WrongParamException("Corrupted acount object");
        }
        final Account entity = new Account();
        entity.setUuid(pojo.getUuid());
        entity.setBalance(pojo.getBalance());
        entity.setCurrency(pojo.getCurrency());
        entity.setDebitEnabled(pojo.isDebitEnabled());
        return entity;
    }

    /**
     * Create new pojo from account entity
     *
     * @param entity account with filled data
     * @return new pojo with all data from entity
     */
    public AccountPojo assemblyPojo(final Account entity) {
        if (entity == null) {
            throw new WrongParamException("Corrupted account object");
        }
        final AccountPojo pojo = new AccountPojo();
        pojo.setUuid(entity.getUuid());
        pojo.setBalance(entity.getBalance());
        pojo.setCurrency(entity.getCurrency());
        pojo.setDebitEnabled(entity.isDebitEnabled());

        // set disposable balance
        double disposableBalance = entity.getBalance();
        for (final Reservation reservation : entity.getReservations()) {
            disposableBalance -= reservation.getSum();
        }
        pojo.setDisposableBalance(disposableBalance);
        return pojo;
    }

}
