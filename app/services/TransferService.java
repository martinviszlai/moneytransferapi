package services;

import data.assemblers.TransferRequestAssembler;
import data.dao.AccountDAO;
import data.dao.ReservationDAO;
import data.dao.TransferRequestDAO;
import data.enums.TransferStatus;
import data.models.Account;
import data.models.Reservation;
import data.models.TransferRequest;
import data.pojos.CreateTransferPojo;
import data.pojos.TransferPojo;
import play.Logger;
import play.db.jpa.JPAApi;
import utils.Constants;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Service for transfers operations
 */
public class TransferService {

    final Logger.ALogger logger = Logger.of(this.getClass());

    private TransferRequestDAO transferRequestDAO;
    private ReservationDAO reservationDAO;
    private AccountDAO accountDAO;
    private TransferRequestAssembler transferRequestAssembler;
    private AccountService accountService;
    private JPAApi jpaApi;

    @Inject
    public TransferService(final TransferRequestDAO transferRequestDAO, final ReservationDAO reservationDAO, final AccountDAO accountDAO, final TransferRequestAssembler transferRequestAssembler, final AccountService accountService, final JPAApi jpaApi) {
        this.transferRequestDAO = transferRequestDAO;
        this.reservationDAO = reservationDAO;
        this.accountDAO = accountDAO;
        this.transferRequestAssembler = transferRequestAssembler;
        this.accountService = accountService;
        this.jpaApi = jpaApi;
    }

    /**
     * Create transfer request. Perform all required validations of fields, check disposable balance on account
     * and create money reservation a transfer request.
     *
     * @param transferPojo details of transfer
     */
    public void createTransferRequest(final CreateTransferPojo transferPojo) {
        // get and validate accounts
        final Account accountFrom = accountDAO.getAccount(transferPojo.getAccountFrom());
        final Account accountTo = accountDAO.getAccount(transferPojo.getAccountTo());

        // convert sum
        final double convertedSum = convertCurrency(transferPojo.getCurrency(), accountFrom.getCurrency(), transferPojo.getSum());

        // check if have enough money
        accountService.checkAccountDisposableBalance(accountFrom, convertedSum);

        // create and persist reservation
        final Reservation reservation = createReservationForTransfer(accountFrom, convertedSum);
        reservationDAO.persistReservation(reservation);

        // create and persist transfer request
        final TransferRequest request = new TransferRequest();
        request.setAccountFrom(accountFrom);
        request.setAccountTo(accountTo);
        request.setCurrency(transferPojo.getCurrency());
        request.setStatus(TransferStatus.OPEN);
        request.setCreateDate(new Date());
        request.setReservation(reservation);
        request.setSum(transferPojo.getSum());
        transferRequestDAO.persistTransferRequest(request);
    }

    /**
     * Get listing of all transfers for account filtered by status
     *
     * @param accountUuid uuid of account which transfers should be searched
     * @param status      status of transfers that should be filtered
     * @return listing of transfers
     */
    public List<TransferPojo> getTransfersForAccount(final String accountUuid, final TransferStatus status) {
        return transferRequestDAO.getTransferRequests(accountUuid, status).stream().map(t -> transferRequestAssembler.assemblyPojo(t)).collect(Collectors.toList());
    }

    /**
     * Process all unprocessed transfer requests stored in DB.
     * <p>
     * Each transfer is processed as separate transaction.
     */
    public void processTransfers() {
        logger.info("Processing stored transfers");
        final List<String> requests = new ArrayList<>();
        jpaApi.withTransaction(() -> {
            requests.addAll(transferRequestDAO.getUnprocessedTransferRequests());
            logger.info("Requests to process: " + requests.size());
        });
        for (final String request : requests) {
            processSingleTransfer(request);
        }
        logger.info("All transfers processed");
    }

    /**
     * Try to process single transfer.
     */
    public void processSingleTransfer(final String transferUuid) {
        try {
            jpaApi.withTransaction(() -> {
                final TransferRequest transfer = transferRequestDAO.getTransferRequest(transferUuid);
                final Account accountFrom = transfer.getAccountFrom();
                final Account accountTo = transfer.getAccountTo();

                // convert sum to correct currencies
                final double sumFrom = convertCurrency(transfer.getCurrency(), accountFrom.getCurrency(), transfer.getSum());
                final double sumTo = convertCurrency(transfer.getCurrency(), accountTo.getCurrency(), transfer.getSum());
                // check if still have enough money on account
                accountService.checkAccountDisposableBalance(accountFrom, sumFrom);
                // remove money from accountFrom
                accountFrom.setBalance(accountFrom.getBalance() - sumFrom);
                accountDAO.persistAccount(accountFrom);
                // add money to accountTo
                accountTo.setBalance(accountTo.getBalance() + sumTo);
                accountDAO.persistAccount(accountTo);
                // delete reservation
                reservationDAO.deleteReservation(transfer.getReservation());
                transfer.setReservation(null);
                // update transfer request
                transfer.setProcessDate(new Date());
                transfer.setStatus(TransferStatus.PROCESSED);
                transferRequestDAO.persistTransferRequest(transfer);
            });
        } catch (final Exception e) {
            logger.warn("Failed to process transfer with uuid" + transferUuid);
            jpaApi.withTransaction(() -> {
                final TransferRequest transfer = transferRequestDAO.getTransferRequest(transferUuid);
                if (transfer.getRetryCount() == Constants.TRANSFER_RETRY_MAX_COUNT) {
                    // reached retry count, mark as failed
                    transfer.setStatus(TransferStatus.FAILED);
                    // also unlock reserved money
                    reservationDAO.deleteReservation(transfer.getReservation());
                    transfer.setReservation(null);
                } else {
                    // increment retry count
                    transfer.setStatus(TransferStatus.UNPROCESSED);
                    transfer.setRetryCount(transfer.getRetryCount() + 1);
                }
                transferRequestDAO.persistTransferRequest(transfer);
            });
        }
    }


    /**
     * Convert sum between currencies
     *
     * @param sourceCurrency code of source currenc
     * @param targetCurrency code of trget currency
     * @param sum            amount of money to be converted
     * @return equivalent sum in target currency
     */
    public double convertCurrency(final String sourceCurrency, final String targetCurrency, final double sum) {
        // for this project, returning same value is enough!
        return sum;
    }

    /**
     * Create reservation entity
     */
    private Reservation createReservationForTransfer(final Account account, final double sum) {
        final Reservation reservation = new Reservation();
        reservation.setAccount(account);
        reservation.setSum(sum);
        reservation.setCreateDate(new Date());

        return reservation;
    }
}
