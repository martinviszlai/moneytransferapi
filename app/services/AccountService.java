package services;

import data.assemblers.AccountAssembler;
import data.dao.AccountDAO;
import data.models.Account;
import data.models.Reservation;
import data.pojos.AccountPojo;
import exceptions.InsufficientResourcesException;
import exceptions.WrongParamException;

import javax.inject.Inject;
import java.util.List;

/**
 * Service for account operations
 */
public class AccountService {

    private AccountDAO accountDAO;
    private AccountAssembler accountAssembler;

    @Inject
    public AccountService(final AccountDAO accountDAO, final AccountAssembler accountAssembler) {
        this.accountDAO = accountDAO;
        this.accountAssembler = accountAssembler;
    }

    /**
     * Create new account in DB
     *
     * @return details of created account
     */
    public AccountPojo createAccount(final AccountPojo account) {
        final Account entity = accountAssembler.assemblyEntity(account);
        return accountAssembler.assemblyPojo(accountDAO.persistAccount(entity));
    }

    /**
     * Retrieve list of all accounts in DB
     *
     * @return list of uuids of all accounts in DB
     */
    public List<String> getAllAccounts() {
        return accountDAO.getAllAccounts();
    }

    /**
     * Get account details
     *
     * @param accountUuid uuid of account
     * @return whole details of account
     */
    public AccountPojo getAccountDetails(final String accountUuid) {
        if (accountUuid == null) {
            throw new WrongParamException("Invalid account uuid!");
        }
        return accountAssembler.assemblyPojo(accountDAO.getAccount(accountUuid));
    }

    /**
     * Check if account has enough money to create new reservation
     *
     * @param account account to be checked
     * @param sum     sum of new reservation
     */
    public void checkAccountDisposableBalance(final Account account, final double sum) {
        if (!account.isDebitEnabled()) { // only if debit is not enabled
            double disposableBalance = account.getBalance();
            for (final Reservation reservation : account.getReservations()) {
                disposableBalance -= reservation.getSum();
            }
            if (disposableBalance - sum < 0) {
                throw new InsufficientResourcesException("Not enough money to perform transfer");
            }
        }
    }
}
