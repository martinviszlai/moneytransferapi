package tasks;

import akka.actor.ActorSystem;
import scala.concurrent.ExecutionContext;
import scala.concurrent.duration.Duration;
import services.TransferService;

import javax.inject.Inject;
import java.util.concurrent.TimeUnit;

public class TransferProcessor {

    private final ActorSystem actorSystem;
    private final ExecutionContext executionContext;

    private TransferService transferService;

    @Inject
    public TransferProcessor(final ActorSystem actorSystem, final ExecutionContext executionContext, final TransferService transferService) {
        this.actorSystem = actorSystem;
        this.executionContext = executionContext;
        this.transferService = transferService;

        this.initialize();
    }


    private void initialize() {
        actorSystem.scheduler().schedule(
            Duration.create(3, TimeUnit.SECONDS), // initialDelay
            Duration.create(30, TimeUnit.SECONDS), // interval
            () -> transferService.processTransfers(),
            executionContext
        );

    }
}