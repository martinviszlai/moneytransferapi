package controllers;

import data.enums.TransferStatus;
import data.pojos.AccountPojo;
import data.pojos.TransferPojo;
import play.db.jpa.Transactional;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import services.AccountService;
import services.TransferService;

import javax.inject.Inject;
import java.util.List;

/**
 * Controller for handling all requests regarding accounts
 */
@Transactional
public class AccountController extends Controller {

    private final AccountService accountService;
    private final TransferService transferService;

    @Inject
    public AccountController(final AccountService accountService, final TransferService transferService) {
        this.accountService = accountService;
        this.transferService = transferService;
    }


    /**
     * Create new account in DB
     *
     * @return details of created account
     */
    public Result createAccount() {
        final AccountPojo requestAccount = Json.fromJson(request().body().asJson(), AccountPojo.class);
        final AccountPojo account = accountService.createAccount(requestAccount);
        return created(Json.toJson(account));
    }

    /**
     * Get account details
     *
     * @param uuid uuid of account
     * @return whole details of account
     */
    public Result getAccountDetails(final String uuid) {
        final AccountPojo account = accountService.getAccountDetails(uuid);
        return ok(Json.toJson(account));
    }

    /**
     * Retrieve list of all accounts in DB
     *
     * @return list of uuids of all accounts in DB
     */
    public Result getAllAccounts() {
        final List<String> accounts = accountService.getAllAccounts();
        return ok(Json.toJson(accounts));
    }

    /**
     * Get all transfers for account specified by param
     *
     * @param uuid   uuid of account
     * @param status status of transfers. If not provided, default value ALL is set when routing.
     * @return all transfers for account with status specified by param
     */
    public Result getTransfers(final String uuid, final String status) {
        final TransferStatus transferStatus;
        if ("ALL".equals(status)) {
            transferStatus = null;
        } else {
            transferStatus = TransferStatus.valueOf(status);
        }
        final List<TransferPojo> transfers = transferService.getTransfersForAccount(uuid, transferStatus);
        return ok(Json.toJson(transfers));
    }
}
