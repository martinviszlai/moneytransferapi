package controllers;

import data.pojos.CreateTransferPojo;
import play.db.jpa.Transactional;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import services.TransferService;

import javax.inject.Inject;

/**
 * Controller for handling all requests regarding transfers
 */
@Transactional
public class TransferController extends Controller {

    private final TransferService transferService;

    @Inject
    public TransferController(final TransferService transferService) {
        this.transferService = transferService;
    }


    /**
     * Create new transfer
     *
     * @return OK status when transfer was successfully submitted
     */
    public Result createTransfer() {
        final CreateTransferPojo requestPojo = Json.fromJson(request().body().asJson(), CreateTransferPojo.class);
        transferService.createTransferRequest(requestPojo);
        return ok();
    }


}
