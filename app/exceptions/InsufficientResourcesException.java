package exceptions;


import play.mvc.Http;
import utils.Constants;

public class InsufficientResourcesException extends MyException {

    public InsufficientResourcesException() {
        super();
    }

    public InsufficientResourcesException(final String message) {
        super(message);
    }

    @Override
    public int getHttpStatusCode() {
        return Http.Status.PRECONDITION_FAILED;
    }


    @Override
    public String getError() {
        return Constants.INSUFFICIENT_RESOURCES;
    }
}
