package exceptions;


/**
 * Generic exception. All custom exceptions should extend this exception.
 */
public abstract class MyException extends RuntimeException {

    public MyException() {
        super();
    }

    public MyException(final String message) {
        super(message);
    }

    public abstract int getHttpStatusCode();

    public abstract String getError();
}
