package exceptions;

import play.Logger;
import play.http.HttpErrorHandler;
import play.libs.Json;
import play.mvc.Http;
import play.mvc.Http.RequestHeader;
import play.mvc.Result;
import play.mvc.Results;
import utils.Constants;

import javax.inject.Singleton;
import java.util.Date;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

/**
 * Override default handling of exceptions.
 * <p>
 * In case of any exception thrown during processing request, return meaningful json with proper
 * status code and basic info, so client can handle errors.
 */
@Singleton
public class ErrorHandler implements HttpErrorHandler {

    final Logger.ALogger logger = Logger.of(this.getClass());

    /**
     * In case of client error, return custom response data
     */
    @Override
    public CompletionStage<Result> onClientError(final RequestHeader request, final int statusCode, final String message) {
        // create response
        final ErrorResponse error = createResponse(request, message);
        error.setErrorMessage(Constants.CLIENT_ERROR);
        error.setStatus(statusCode);

        // pass created response object
        return CompletableFuture.completedFuture(
            Results.status(error.getStatus(), Json.toJson(error))
        );
    }

    /**
     * In case of custom exception, return status code defined in exception.
     * In case of other exceptions, convert them into internal server error.
     */
    @Override
    public CompletionStage<Result> onServerError(final RequestHeader request, final Throwable exception) {
        // log exception with stacktrace (length of stacktrace configured in logback.xml)
        logger.error(exception.getMessage(), exception);

        // create response
        final ErrorResponse error = createResponse(request, exception.getMessage());
        if (exception instanceof MyException) {
            error.setErrorMessage(((MyException) exception).getError());
            error.setStatus(((MyException) exception).getHttpStatusCode());
        } else {
            error.setErrorMessage(Constants.INTERNAL_ERROR);
            error.setStatus(Http.Status.INTERNAL_SERVER_ERROR);
        }

        // pass created response object
        return CompletableFuture.completedFuture(
            Results.status(error.getStatus(), Json.toJson(error))
        );
    }

    /**
     * Create response pojo and set common fields
     */
    private ErrorResponse createResponse(final RequestHeader request, final String message) {
        final ErrorResponse error = new ErrorResponse();

        final Optional<String> pathHeader = request.getHeaders().get("Raw-Request-URI");
        if (pathHeader.isPresent()) {
            error.setPath(pathHeader.get());
        }
        error.setError(message);
        error.setTimestamp(new Date().getTime());
        return error;
    }

}
