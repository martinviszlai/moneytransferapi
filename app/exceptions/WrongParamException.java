package exceptions;


import play.mvc.Http;
import utils.Constants;

public class WrongParamException extends MyException {

    public WrongParamException() {
        super();
    }

    public WrongParamException(final String message) {
        super(message);
    }

    @Override
    public int getHttpStatusCode() {
        return Http.Status.PRECONDITION_FAILED;
    }


    @Override
    public String getError() {
        return Constants.WRONG_PARAM;
    }
}
