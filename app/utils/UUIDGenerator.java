package utils;

import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.id.IdentifierGenerator;

import java.io.Serializable;
import java.util.UUID;

public class UUIDGenerator implements IdentifierGenerator {

    /**
     * This method will generate a random uuid and return it, hibernate can use
     * this id as it generator class id.
     */
    @Override
    public Serializable generate(final SharedSessionContractImplementor session, final Object object) throws HibernateException {
        final UUID uuid = UUID.randomUUID();
        return Long.toString(Math.abs(uuid.getMostSignificantBits()), 36) + Long.toString(Math.abs(uuid.getLeastSignificantBits()), 36);
    }
}
