package utils;

/**
 * Generic constants used across whole project
 */
public class Constants {

    private static final String ERROR_MSG_PREFIX = "ERROR_";
    public static final String WRONG_PARAM = ERROR_MSG_PREFIX + "WRONG_PARAM";
    public static final String INSUFFICIENT_RESOURCES = ERROR_MSG_PREFIX + "INSUFFICIENT_RESOURCES";
    public static final String INTERNAL_ERROR = ERROR_MSG_PREFIX + "INTERNAL_SERVER_ERROR";
    public static final String CLIENT_ERROR = ERROR_MSG_PREFIX + "CLIENT_ERROR";

    public static final int TRANSFER_RETRY_MAX_COUNT = 3;
}
