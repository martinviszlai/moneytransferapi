#Money transfer API
This is project for Revolut Java/Scala Test by Martin Viszlai. 
Project is build on Play Framework 2.6.5, H2 database, Hibernate and JPA. For jUnit testing is used Mockito.


## Basic workflow
Main point is to transfer money between accounts. This is started by sending `POST` request to endpoint `/transfer/`.
Server first calculate sum to currency in which is account from which money are being sent. 
Then, verification if account has enough money to make requested transaction and if not, an error response is returned.
After verification of account balance, money for transactions are reserved, thus not available for other transactions.
Besides creating reservation, transaction request is also created and stored. Transaction requests are handled 
separately, in this case by cron job. This way, we can handle order of processing transactions and can recover in case 
of any failure. After successful transaction processing, both accounts have updated balance, money reservation is removed
and transaction request is marked as processed. In case of failure, request is marked unprocessed and retry count 
is incremented, thus request will be processed again. Reservation is still active. Oly when retry count reaches limit, 
transaction request is marked as failed and money reservation is removed.    

### Few notes
To keep project simple, I decided to:
* use DB and "cron job" for queueing transaction requests. Some better mechanism like RabbitMQ or Kafka can be used.
* consider all errors during processing transaction as recoverable 
* use simplified package structure, skipped interfaces for DAOs and services and provided only implementation classes. 
* skip currency conversion. Method for converting currencies is simply returning the same value
* all actions are performed as single transactions
* account balance is represented as double, what is not precise enough for production    

## Installation
To run the application, unzip the file on the target path, and then run the script in the `bin` directory. There are two 
versions, a bash shell script and a windows `.bat` script. 
To run script on linux set script as executable:
```
$ chmod +x /path/to/bin/<project-name>
```

Launching application requires to provide application secret as parameter of script. Application secret for this project
 is `revolut`. Starting server then looks like:
 ```
 $ moneytransferapi/bin/moneytransferapi -Dplay.http.secret.key=revolut
 ```

## API Reference
All endpoints can be tested via Postman. Here is collection `https://www.getpostman.com/collections/764a14bf4f217639275c`.
Collection uses environment variables, so an environment must be selected to store them.

If you do not have postman, you can test communicate with API via curl. Available endpoints are:
#### get all accounts
Retrieve listing of identifiers all accounts in DB. Two accounts are created automatically.
```
curl -X GET http://localhost:9000/account/
```
#### create new account
Create new account with provided data.
```
curl -X POST http://localhost:9000/account/ -d '{
	"balance":2500,
	"currency":"USD",
	"debit_enabled":true
}'
```
#### get account details
Get details of account.
```
curl -X GET http://localhost:9000/account/16omrbidpprlb1iju8r1btbmb0
```
#### create transfer
Create new money transfer. Transfer is not processed immediately (see Basic workflow), so when you check account details
 of account from money are being sent, you will see that money for created transaction are reserved.
```
curl -X POST http://localhost:9000/transfer/ -d '{
	"sum":1000,
	"account_from":"16omrbidpprlb1iju8r1btbmb0",
	"account_to":"18t9w9mx2t24e1ii3hsyltaw4v",
	"currency":"EUR"
}'
```
#### get transfers for account
Get all transfers for account. You can see status of each transfer. This status is changed when transaction is being processed.
```
curl -X GET http://localhost:9000/account/16omrbidpprlb1iju8r1btbmb0/transfer
```
## Tests
Postman collection is with simple tests, so whole collection can be tested by collection runner.

To run all jUnit tests, sbt (http://www.scala-sbt.org/) must be installed on computer. 
Then all tests can be un by command `sbt test`.

## Initial Test Requirements
Design and implement a RESTful API (including data model and the backing implementation) for money transfers between accounts. 

**Explicit requirements:**
1. keep it simple and to the point (e.g. no need to implement any authentication, assume the APi is invoked by another internal system/service)
2. use whatever frameworks/libraries you like (except Spring, sorry!) but don't forget about the requirement #1
3. the datastore should run in-memory for the sake of this test
4. the final result should be executable as a standalone program (should not require a pre-installed container/server)
5. demonstrate with tests that the API works as expected

**Implicit requirements:**
1. the code produced by you is expected to be of high quality.
2. there are no detailed requirements, use common sense.