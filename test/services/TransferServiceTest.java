package services;

import data.assemblers.TransferRequestAssembler;
import data.dao.AccountDAO;
import data.dao.ReservationDAO;
import data.dao.TransferRequestDAO;
import data.enums.TransferStatus;
import data.models.Account;
import data.models.Reservation;
import data.models.TransferRequest;
import data.pojos.CreateTransferPojo;
import data.pojos.TransferPojo;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import play.Application;
import play.db.jpa.JPAApi;
import play.test.Helpers;
import play.test.WithApplication;
import utils.Constants;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class TransferServiceTest extends WithApplication {

    @Override
    public Application provideApplication() {
        return Helpers.fakeApplication(Helpers.inMemoryDatabase());
    }

    private TransferRequestDAO transferRequestDAO = mock(TransferRequestDAO.class);
    private ReservationDAO reservationDAO = mock(ReservationDAO.class);
    private AccountDAO accountDAO = mock(AccountDAO.class);
    private TransferRequestAssembler transferRequestAssembler = mock(TransferRequestAssembler.class);
    private AccountService accountService = mock(AccountService.class);
    private JPAApi jpaApi;

    private TransferService transferService;

    private CreateTransferPojo createTransferPojo;
    private TransferPojo transferPojo;
    private TransferRequest transferRequest;
    private final String ACCOUNT_UUID = "uuid1";
    private final double INITIAL_ACCOUNT_BALLANCE = 5;
    private Account accountFrom;
    private Account accountTo;
    private Reservation reservation;

    @Before
    public void init() {
        jpaApi = app.injector().instanceOf(JPAApi.class);
        transferService = spy(new TransferService(transferRequestDAO, reservationDAO, accountDAO, transferRequestAssembler, accountService, jpaApi));

        final Date createDate = new Date();
        createTransferPojo = new CreateTransferPojo();
        createTransferPojo.setCurrency("EUR");
        createTransferPojo.setSum(5);
        createTransferPojo.setAccountFrom("account_from_uuid");
        createTransferPojo.setAccountTo("account_to_uuid");

        transferPojo = new TransferPojo();
        transferPojo.setReservation("reservation_uuid");
        transferPojo.setStatus(TransferStatus.OPEN);
        transferPojo.setCreateDate(createDate.getTime());
        transferPojo.setCurrency("EUR");
        transferPojo.setSum(5);
        transferPojo.setAccountFrom("account_from_uuid");
        transferPojo.setAccountTo("account_to_uuid");

        reservation = new Reservation();
        reservation.setUuid("reservation_uuid");

        accountFrom = new Account();
        accountFrom.setUuid("account_from_uuid");
        accountFrom.setBalance(INITIAL_ACCOUNT_BALLANCE);
        accountFrom.setDebitEnabled(false);
        accountFrom.setCurrency("eur");
        accountTo = new Account();
        accountTo.setUuid("account_to_uuid");
        accountTo.setBalance(INITIAL_ACCOUNT_BALLANCE);
        accountTo.setDebitEnabled(false);
        accountTo.setCurrency("eur");

        transferRequest = new TransferRequest();
        transferRequest.setUuid("uuid1");
        transferRequest.setReservation(reservation);
        transferRequest.setStatus(TransferStatus.OPEN);
        transferRequest.setCreateDate(createDate);
        transferRequest.setCurrency("EUR");
        transferRequest.setSum(5);
        transferRequest.setAccountFrom(accountFrom);
        transferRequest.setAccountTo(accountTo);
        transferRequest.setRetryCount(0);
    }

    @Test
    public void testGetTransfersForAccount() {
        when(transferRequestDAO.getTransferRequests(any(), any())).thenReturn(Arrays.asList(transferRequest));
        when(transferRequestAssembler.assemblyPojo(any())).thenReturn(transferPojo);

        transferService.getTransfersForAccount(ACCOUNT_UUID, TransferStatus.OPEN);

        verify(transferRequestDAO, times(1)).getTransferRequests(eq(ACCOUNT_UUID), eq(TransferStatus.OPEN));
        verify(transferRequestAssembler, times(1)).assemblyPojo(eq(transferRequest));
    }

    @Test
    public void testGetTransfersForAccount_nullParams() {
        when(transferRequestDAO.getTransferRequests(any(), any())).thenReturn(Arrays.asList(transferRequest));
        when(transferRequestAssembler.assemblyPojo(any())).thenReturn(transferPojo);

        transferService.getTransfersForAccount(null, null);

        verify(transferRequestDAO, times(1)).getTransferRequests(eq(null), eq(null));
        verify(transferRequestAssembler, times(1)).assemblyPojo(eq(transferRequest));
    }

    @Test
    public void testCreateTransferRequest() {
        when(accountDAO.getAccount(accountFrom.getUuid())).thenReturn(accountFrom);
        when(accountDAO.getAccount(accountTo.getUuid())).thenReturn(accountTo);

        transferService.createTransferRequest(createTransferPojo);

        verify(accountDAO, times(1)).getAccount(eq(accountFrom.getUuid()));
        verify(accountDAO, times(1)).getAccount(eq(accountTo.getUuid()));

        final ArgumentCaptor<Reservation> reservationCaptor = ArgumentCaptor.forClass(Reservation.class);
        verify(reservationDAO, times(1)).persistReservation(reservationCaptor.capture());
        final Reservation storedReservation = reservationCaptor.getValue();
        assertEquals(accountFrom, storedReservation.getAccount());
        assertNotNull(storedReservation.getCreateDate());
        assertNotNull(storedReservation.getSum());

        final ArgumentCaptor<TransferRequest> transferCaptor = ArgumentCaptor.forClass(TransferRequest.class);
        verify(transferRequestDAO, times(1)).persistTransferRequest(transferCaptor.capture());
        final TransferRequest transferRequest = transferCaptor.getValue();
        assertEquals(accountFrom, transferRequest.getAccountFrom());
        assertEquals(accountTo, transferRequest.getAccountTo());
        assertEquals(createTransferPojo.getCurrency(), transferRequest.getCurrency());
        assertEquals(TransferStatus.OPEN, transferRequest.getStatus());
        assertNotNull(transferRequest.getSum());
        assertNotNull(transferRequest.getReservation());
        assertNotNull(transferRequest.getCreateDate());
        assertEquals(0, transferRequest.getRetryCount());
        assertNull(transferRequest.getProcessDate());

    }

    @Test
    public void testProcessTransfers() {
        final List<String> transferUuids = Arrays.asList("uuid1", "uuid2");
        when(transferRequestDAO.getUnprocessedTransferRequests()).thenReturn(transferUuids);
        doNothing().when(transferService).processSingleTransfer(anyString());

        transferService.processTransfers();

        verify(transferRequestDAO, times(1)).getUnprocessedTransferRequests();
        verify(transferService, times(transferUuids.size())).processSingleTransfer(anyString());
        for (final String uuid : transferUuids) {
            verify(transferService, times(1)).processSingleTransfer(eq(uuid));
        }
    }

    @Test
    public void testProcessSingleTransfer() {
        when(transferRequestDAO.getTransferRequest(anyString())).thenReturn(transferRequest);

        transferService.processSingleTransfer(transferRequest.getUuid());

        verify(accountService, times(1)).checkAccountDisposableBalance(eq(transferRequest.getAccountFrom()), anyDouble());

        final ArgumentCaptor<Account> accountArgumentCaptor = ArgumentCaptor.forClass(Account.class);
        verify(accountDAO, times(2)).persistAccount(accountArgumentCaptor.capture());
        final List<Account> accounts = accountArgumentCaptor.getAllValues();
        assertEquals(transferRequest.getAccountFrom().getUuid(), accounts.get(0).getUuid());
        assertTrue(accounts.get(0).getBalance() < INITIAL_ACCOUNT_BALLANCE);
        assertEquals(transferRequest.getAccountTo().getUuid(), accounts.get(1).getUuid());
        assertTrue(accounts.get(1).getBalance() > INITIAL_ACCOUNT_BALLANCE);

        verify(reservationDAO, times(1)).deleteReservation(eq(reservation));

        final ArgumentCaptor<TransferRequest> requestArgumentCaptor = ArgumentCaptor.forClass(TransferRequest.class);
        verify(transferRequestDAO, times(1)).persistTransferRequest(requestArgumentCaptor.capture());
        final TransferRequest storedRequest = requestArgumentCaptor.getValue();
        assertEquals(TransferStatus.PROCESSED, storedRequest.getStatus());
        assertNull(storedRequest.getReservation());
        assertNotNull(storedRequest.getProcessDate());
    }

    @Test
    public void testProcessSingleTransfer_errorDuringProcessing_enoughRetryCount() {
        when(transferRequestDAO.getTransferRequest(anyString())).thenReturn(transferRequest);
        doThrow(new RuntimeException()).when(accountDAO).persistAccount(any());

        transferService.processSingleTransfer(transferRequest.getUuid());

        final ArgumentCaptor<TransferRequest> requestArgumentCaptor = ArgumentCaptor.forClass(TransferRequest.class);
        verify(transferRequestDAO, times(1)).persistTransferRequest(requestArgumentCaptor.capture());
        final TransferRequest storedRequest = requestArgumentCaptor.getValue();
        assertEquals(TransferStatus.UNPROCESSED, storedRequest.getStatus());
        assertNotNull(storedRequest.getReservation());
        assertNull(storedRequest.getProcessDate());
        assertEquals(transferRequest.getRetryCount() + 1, storedRequest.getRetryCount() + 1);
    }

    @Test
    public void testProcessSingleTransfer_errorDuringProcessing_tooManyRetryCount() {
        transferRequest.setRetryCount(Constants.TRANSFER_RETRY_MAX_COUNT);
        when(transferRequestDAO.getTransferRequest(anyString())).thenReturn(transferRequest);
        doThrow(new RuntimeException()).when(accountDAO).persistAccount(any());

        transferService.processSingleTransfer(transferRequest.getUuid());

        final ArgumentCaptor<TransferRequest> requestArgumentCaptor = ArgumentCaptor.forClass(TransferRequest.class);
        verify(transferRequestDAO, times(1)).persistTransferRequest(requestArgumentCaptor.capture());
        verify(reservationDAO, times(1)).deleteReservation(eq(reservation));
        final TransferRequest storedRequest = requestArgumentCaptor.getValue();
        assertEquals(TransferStatus.FAILED, storedRequest.getStatus());
        assertNull(storedRequest.getReservation());
        assertNull(storedRequest.getProcessDate());
    }

}
