package services;

import data.assemblers.AccountAssembler;
import data.dao.AccountDAO;
import data.models.Account;
import data.models.Reservation;
import data.pojos.AccountPojo;
import exceptions.InsufficientResourcesException;
import exceptions.WrongParamException;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.internal.matchers.apachecommons.ReflectionEquals;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class AccountServiceTest {

    private AccountDAO accountDAO = mock(AccountDAO.class);
    private AccountAssembler accountAssembler = mock(AccountAssembler.class);

    private AccountService accountService = new AccountService(accountDAO, accountAssembler);

    private AccountPojo accountPojo;
    private Account accountEntity;
    private final String ACCOUNT_UUID = "uuid1";

    @Before
    public void init() {
        accountPojo = new AccountPojo();
        accountPojo.setUuid("uuid1");
        accountPojo.setCurrency("EUR");
        accountPojo.setBalance(5);
        accountPojo.setDebitEnabled(true);

        final Reservation reservation = new Reservation();
        reservation.setSum(2);

        accountEntity = new Account();
        accountEntity.setUuid("uuid1");
        accountEntity.setCurrency("EUR");
        accountEntity.setBalance(5);
        accountEntity.setDebitEnabled(false);
        accountEntity.setReservations(Arrays.asList(reservation));

        when(accountAssembler.assemblyPojo(accountEntity)).thenReturn(accountPojo);
        when(accountAssembler.assemblyEntity(accountPojo)).thenReturn(accountEntity);
    }

    @Test
    public void testCreateAccount() {
        when(accountDAO.persistAccount(any(Account.class))).thenReturn(accountEntity);

        final AccountPojo result = accountService.createAccount(accountPojo);

        final ArgumentCaptor<AccountPojo> pojoArgumentCaptor = ArgumentCaptor.forClass(AccountPojo.class);
        final ArgumentCaptor<Account> entityArgumentCaptor = ArgumentCaptor.forClass(Account.class);

        verify(accountAssembler, times(1)).assemblyEntity(pojoArgumentCaptor.capture());
        assertTrue(new ReflectionEquals(accountPojo).matches(pojoArgumentCaptor.getValue()));

        verify(accountAssembler, times(1)).assemblyPojo(entityArgumentCaptor.capture());
        assertTrue(new ReflectionEquals(accountEntity).matches(entityArgumentCaptor.getValue()));

        verify(accountDAO, times(1)).persistAccount(entityArgumentCaptor.capture());
        assertTrue(new ReflectionEquals(accountEntity).matches(entityArgumentCaptor.getValue()));

        assertTrue(new ReflectionEquals(accountPojo).matches(result));
    }

    @Test
    public void testGetAccounts() {
        final List<String> uuids = Arrays.asList("uuid1", "uuid2");
        when(accountDAO.getAllAccounts()).thenReturn(uuids);

        final List<String> result = accountService.getAllAccounts();

        verify(accountDAO, times(1)).getAllAccounts();
        assertEquals(uuids, result);
    }

    @Test
    public void testGetAccountDetails() {
        when(accountDAO.getAccount(ACCOUNT_UUID)).thenReturn(accountEntity);

        final AccountPojo result = accountService.getAccountDetails(ACCOUNT_UUID);

        verify(accountDAO, times(1)).getAccount(ACCOUNT_UUID);
        assertTrue(new ReflectionEquals(accountPojo).matches(result));
    }

    @Test
    public void testGetAccountDetails_nullParam() {
        when(accountDAO.getAccount(ACCOUNT_UUID)).thenReturn(accountEntity);

        try {
            accountService.getAccountDetails(null);
            fail();
        } catch (final WrongParamException e) {

        } finally {
            verify(accountDAO, never()).getAccount(any());
        }
    }

    @Test
    public void testCheckAccountDisposableBalance_debitDisabled_enoughBalance() {
        accountService.checkAccountDisposableBalance(accountEntity, 1);
    }

    @Test
    public void testCheckAccountDisposableBalance_debitEnabled() {
        accountEntity.setDebitEnabled(true);
        accountService.checkAccountDisposableBalance(accountEntity, 10);
    }

    @Test(expected = InsufficientResourcesException.class)
    public void testCheckAccountDisposableBalance_debitDisabled_insufficientBalance() {
        accountService.checkAccountDisposableBalance(accountEntity, 10);
    }
}
