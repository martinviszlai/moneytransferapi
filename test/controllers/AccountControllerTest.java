package controllers;

import data.enums.TransferStatus;
import data.pojos.AccountPojo;
import data.pojos.TransferPojo;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.internal.matchers.apachecommons.ReflectionEquals;
import play.core.j.JavaContextComponents;
import play.core.j.JavaHelpers;
import play.libs.Json;
import play.mvc.Http;
import play.mvc.Result;
import play.test.Helpers;
import services.AccountService;
import services.TransferService;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static play.mvc.Http.Status.OK;
import static play.test.Helpers.*;

public class AccountControllerTest implements JavaHelpers {

    private final AccountService accountService = mock(AccountService.class);
    private final TransferService transferService = mock(TransferService.class);

    private final AccountController accountController = new AccountController(accountService, transferService);

    private final String ACCOUNT_UUID = "uuid1";
    private JavaContextComponents contextComponents;
    private AccountPojo accountPojo;
    private TransferPojo transfer;
    private List<TransferPojo> transferPojos;

    @Before
    public void init() {
        contextComponents = createContextComponents();

        accountPojo = new AccountPojo();
        accountPojo.setUuid(ACCOUNT_UUID);
        accountPojo.setCurrency("EUR");
        accountPojo.setBalance(5);
        accountPojo.setDebitEnabled(true);

        transfer = new TransferPojo();
        transfer.setReservation("reservation_uuid");
        transfer.setStatus(TransferStatus.OPEN);
        transfer.setCreateDate(new Date().getTime());
        transfer.setCurrency("EUR");
        transfer.setSum(5);
        transfer.setAccountFrom("account_from_uuid");
        transfer.setAccountTo("account_to_uuid");
        transferPojos = Arrays.asList(transfer);
    }

    private String convertToString(final Object obj) {
        return Json.toJson(obj).toString();
    }

    @Test
    public void testCreateAccount() {
        when(accountService.createAccount(any(AccountPojo.class))).thenReturn(accountPojo);

        final Http.RequestBuilder request = new Http.RequestBuilder()
            .method(POST)
            .uri("/account/")
            .bodyJson(Json.toJson(accountPojo));

        final Result result = new Helpers().invokeWithContext(request, contextComponents, () -> accountController.createAccount());
        // check response
        assertEquals(CREATED, result.status());
        assertEquals(Http.MimeTypes.JSON, result.contentType().get());
        assertEquals(convertToString(accountPojo), contentAsString(result));

        // check calls
        final ArgumentCaptor<AccountPojo> createAccountCaptor = ArgumentCaptor.forClass(AccountPojo.class);
        verify(accountService, times(1)).createAccount(createAccountCaptor.capture());
        assertTrue(new ReflectionEquals(accountPojo).matches(createAccountCaptor.getValue()));
    }

    @Test
    public void testGetAccountDetails() {
        when(accountService.getAccountDetails(ACCOUNT_UUID)).thenReturn(accountPojo);

        final Http.RequestBuilder request = new Http.RequestBuilder()
            .method(GET)
            .uri("/account/" + ACCOUNT_UUID);

        final Result result = new Helpers().invokeWithContext(request, contextComponents, () -> accountController.getAccountDetails(ACCOUNT_UUID));
        // check response
        assertEquals(OK, result.status());
        assertEquals(Http.MimeTypes.JSON, result.contentType().get());
        assertEquals(convertToString(accountPojo), contentAsString(result));

        // check calls
        final ArgumentCaptor<String> getAccountDetailsCaptor = ArgumentCaptor.forClass(String.class);
        verify(accountService, times(1)).getAccountDetails(getAccountDetailsCaptor.capture());
        assertEquals(ACCOUNT_UUID, getAccountDetailsCaptor.getValue());
    }

    @Test
    public void testGetAllAccounts() {
        final List<String> uuids = Arrays.asList("uuid1", "uuid2");
        when(accountService.getAllAccounts()).thenReturn(uuids);

        final Http.RequestBuilder request = new Http.RequestBuilder()
            .method(GET)
            .uri("/account/");

        final Result result = new Helpers().invokeWithContext(request, contextComponents, () -> accountController.getAllAccounts());
        // check response
        assertEquals(OK, result.status());
        assertEquals(Http.MimeTypes.JSON, result.contentType().get());
        assertEquals(convertToString(uuids), contentAsString(result));

        // check calls
        verify(accountService, times(1)).getAllAccounts();
    }

    @Test
    public void testGetTransactions() {
        when(transferService.getTransfersForAccount(any(), any())).thenReturn(transferPojos);

        final Http.RequestBuilder request = new Http.RequestBuilder()
            .method(GET)
            .uri("/account/" + ACCOUNT_UUID + "/transaction");

        final Result result = new Helpers().invokeWithContext(request, contextComponents, () -> accountController.getTransfers(ACCOUNT_UUID, "OPEN"));
        // check response
        assertEquals(OK, result.status());
        assertEquals(Http.MimeTypes.JSON, result.contentType().get());
        assertEquals(convertToString(transferPojos), contentAsString(result));

        // check calls
        verify(transferService, times(1)).getTransfersForAccount(eq(ACCOUNT_UUID), eq(TransferStatus.OPEN));
    }

    @Test
    public void testGetTransactions_allStatusParam() {
        when(transferService.getTransfersForAccount(any(), any())).thenReturn(transferPojos);

        final Http.RequestBuilder request = new Http.RequestBuilder()
            .method(GET)
            .uri("/account/" + ACCOUNT_UUID + "/transaction");

        final Result result = new Helpers().invokeWithContext(request, contextComponents, () -> accountController.getTransfers(ACCOUNT_UUID, "ALL"));
        // check response
        assertEquals(OK, result.status());
        assertEquals(Http.MimeTypes.JSON, result.contentType().get());
        assertEquals(convertToString(transferPojos), contentAsString(result));

        // check calls
        verify(transferService, times(1)).getTransfersForAccount(eq(ACCOUNT_UUID), eq(null));
    }
}
