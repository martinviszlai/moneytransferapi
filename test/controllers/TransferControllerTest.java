package controllers;

import data.pojos.CreateTransferPojo;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.internal.matchers.apachecommons.ReflectionEquals;
import play.core.j.JavaContextComponents;
import play.core.j.JavaHelpers;
import play.libs.Json;
import play.mvc.Http;
import play.mvc.Result;
import play.test.Helpers;
import services.TransferService;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;
import static play.mvc.Http.Status.OK;
import static play.test.Helpers.POST;

public class TransferControllerTest implements JavaHelpers {

    private final TransferService transferService = mock(TransferService.class);

    private final TransferController transferController = new TransferController(transferService);

    private JavaContextComponents contextComponents;
    private CreateTransferPojo transfer;

    @Before
    public void init() {
        contextComponents = createContextComponents();

        transfer = new CreateTransferPojo();
        transfer.setCurrency("EUR");
        transfer.setSum(5);
        transfer.setAccountFrom("account_from_uuid");
        transfer.setAccountTo("account_to_uuid");
    }

    @Test
    public void testCreateTransfer() {

        final Http.RequestBuilder request = new Http.RequestBuilder()
            .method(POST)
            .uri("/transfer/")
            .bodyJson(Json.toJson(transfer));

        final Result result = new Helpers().invokeWithContext(request, contextComponents, () -> transferController.createTransfer());
        // check response
        assertEquals(OK, result.status());

        // check calls
        final ArgumentCaptor<CreateTransferPojo> createTransferCaptor = ArgumentCaptor.forClass(CreateTransferPojo.class);
        verify(transferService, times(1)).createTransferRequest(createTransferCaptor.capture());
        assertTrue(new ReflectionEquals(transfer).matches(createTransferCaptor.getValue()));
    }


}
