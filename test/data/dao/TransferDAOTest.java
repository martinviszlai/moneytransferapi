package data.dao;

import data.enums.TransferStatus;
import data.models.Account;
import data.models.Reservation;
import data.models.TransferRequest;
import org.junit.Before;
import org.junit.Test;
import org.mockito.internal.matchers.apachecommons.ReflectionEquals;
import play.Application;
import play.db.jpa.JPAApi;
import play.test.Helpers;
import play.test.WithApplication;

import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

public class TransferDAOTest extends WithApplication {

    @Override
    public Application provideApplication() {
        return Helpers.fakeApplication(Helpers.inMemoryDatabase());
    }

    JPAApi jpaApi;
    TransferRequestDAO transferRequestDAO;
    private final String ACCOUNT_UUID = "16omrbidpprlb1iju8r1btbmb0";
    private TransferRequest openEntity;
    private TransferRequest unprocessedEntity;
    private TransferRequest failedEntity;

    @Before
    public void init() {
        // must get instances like this, as application is created for each test
        jpaApi = app.injector().instanceOf(JPAApi.class);
        transferRequestDAO = app.injector().instanceOf(TransferRequestDAO.class);

        // no need to setup / clear DB, WithApplication create new DB for each test
    }

    private void performNativeQuery(final String query) {
        jpaApi.withTransaction(em -> em.createNativeQuery(query).executeUpdate());
    }


    @Test
    public void testPersistTransferRequest() {
        performNativeQuery("DELETE FROM transfer_request");

        //accounts from evolutions
        final Account accountFrom = jpaApi.withTransaction(em -> (Account) em.createQuery("SELECT acc FROM Account acc WHERE acc.uuid LIKE '16omrbidpprlb1iju8r1btbmb0'").getSingleResult());
        final Account accountTo = jpaApi.withTransaction(em -> (Account) em.createQuery("SELECT acc FROM Account acc WHERE acc.uuid LIKE '18t9w9mx2t24e1ii3hsyltaw4v'").getSingleResult());
        final Reservation reservation = new Reservation();
        reservation.setSum(5);
        reservation.setCreateDate(new Date());
        reservation.setAccount(accountFrom);
        final TransferRequest entity = new TransferRequest();
        entity.setAccountFrom(accountFrom);
        entity.setAccountTo(accountTo);
        entity.setCurrency("EUR");
        entity.setStatus(TransferStatus.OPEN);
        entity.setCreateDate(new Date());
        entity.setReservation(reservation);
        entity.setSum(5);

        final TransferRequest[] transferRequest = new TransferRequest[1];
        jpaApi.withTransaction(() -> {
            jpaApi.em().persist(reservation); //save new reservation
            transferRequest[0] = transferRequestDAO.persistTransferRequest(entity);
        });
        // check returned transfer request
        assertNotNull(transferRequest[0]);
        assertNotNull(transferRequest[0].getUuid()); // uuid was generated
        assertTrue(new ReflectionEquals(entity, "uuid", "accountFrom", "accountTo", "reservation").matches(transferRequest[0]));

        // check stored transfer request
        final TransferRequest storedTransfer = jpaApi.withTransaction(em -> (TransferRequest) em.createQuery("SELECT req FROM TransferRequest req").getSingleResult());
        assertTrue(new ReflectionEquals(transferRequest[0], "accountFrom", "accountTo", "reservation").matches(storedTransfer));
        assertEquals(accountFrom.getUuid(), storedTransfer.getAccountFrom().getUuid());
        assertEquals(accountTo.getUuid(), storedTransfer.getAccountTo().getUuid());
        assertEquals(reservation.getUuid(), storedTransfer.getReservation().getUuid());
    }

    @Test
    public void testGetTransferRequests_nullStatus() {
        performNativeQuery("DELETE FROM transfer_request");
        prepareTransferRequests();

        jpaApi.withTransaction(() -> {
            final List<TransferRequest> result = transferRequestDAO.getTransferRequests(ACCOUNT_UUID, null);

            assertTrue(result.size() == 3); // all 3 requests
        });
    }

    @Test
    public void testGetTransferRequests_providedStatus() {
        performNativeQuery("DELETE FROM transfer_request");
        prepareTransferRequests();

        jpaApi.withTransaction(() -> {
            final List<TransferRequest> result = transferRequestDAO.getTransferRequests(ACCOUNT_UUID, TransferStatus.FAILED);

            assertTrue(result.size() == 1); // only failed request
            assertEquals(failedEntity.getUuid(), result.get(0).getUuid());
        });
    }

    @Test
    public void testGetTransferRequest() {
        performNativeQuery("DELETE FROM transfer_request");
        prepareTransferRequests();

        jpaApi.withTransaction(() -> {
            final TransferRequest result = transferRequestDAO.getTransferRequest(openEntity.getUuid());

            assertEquals(openEntity.getUuid(), result.getUuid());
        });
    }

    @Test
    public void testGetTransferRequest_invalidUuid() {
        performNativeQuery("DELETE FROM transfer_request");
        prepareTransferRequests();

        jpaApi.withTransaction(() -> {
            final TransferRequest result = transferRequestDAO.getTransferRequest("invalid_uuid");

            assertNull(result);
        });
    }

    @Test
    public void testGetUnprocessedTransferRequests() {
        performNativeQuery("DELETE FROM transfer_request");
        prepareTransferRequests();

        jpaApi.withTransaction(() -> {
            final List<String> result = transferRequestDAO.getUnprocessedTransferRequests();

            assertTrue(result.size() == 2); // only open and unprocessed request
            assertEquals(openEntity.getUuid(), result.get(0));
            assertEquals(unprocessedEntity.getUuid(), result.get(1));
        });
    }

    private void prepareTransferRequests() {
        //accounts from evolutions
        final Account accountFrom = jpaApi.withTransaction(em -> (Account) em.createQuery("SELECT acc FROM Account acc WHERE acc.uuid LIKE '16omrbidpprlb1iju8r1btbmb0'").getSingleResult());
        final Account accountTo = jpaApi.withTransaction(em -> (Account) em.createQuery("SELECT acc FROM Account acc WHERE acc.uuid LIKE '18t9w9mx2t24e1ii3hsyltaw4v'").getSingleResult());

        final Reservation reservation = new Reservation();
        reservation.setSum(5);
        reservation.setCreateDate(new Date());
        reservation.setAccount(accountFrom);

        openEntity = new TransferRequest();
        openEntity.setAccountFrom(accountFrom);
        openEntity.setAccountTo(accountTo);
        openEntity.setCurrency("EUR");
        openEntity.setStatus(TransferStatus.OPEN);
        openEntity.setCreateDate(new Date());
        openEntity.setReservation(reservation);
        openEntity.setSum(5);

        unprocessedEntity = new TransferRequest();
        unprocessedEntity.setAccountFrom(accountFrom);
        unprocessedEntity.setAccountTo(accountTo);
        unprocessedEntity.setCurrency("EUR");
        unprocessedEntity.setStatus(TransferStatus.UNPROCESSED);
        unprocessedEntity.setCreateDate(new Date());
        unprocessedEntity.setReservation(reservation);
        unprocessedEntity.setSum(5);

        failedEntity = new TransferRequest();
        failedEntity.setAccountFrom(accountTo);
        failedEntity.setAccountTo(accountFrom);
        failedEntity.setCurrency("EUR");
        failedEntity.setStatus(TransferStatus.FAILED);
        failedEntity.setCreateDate(new Date());
        failedEntity.setReservation(reservation);
        failedEntity.setSum(5);

        jpaApi.withTransaction(() -> {
            jpaApi.em().persist(reservation);
            jpaApi.em().persist(openEntity);
            jpaApi.em().persist(unprocessedEntity);
            jpaApi.em().persist(failedEntity);
        });
    }


}
