package data.dao;

import data.models.Account;
import data.models.Reservation;
import org.junit.Before;
import org.junit.Test;
import org.mockito.internal.matchers.apachecommons.ReflectionEquals;
import play.Application;
import play.db.jpa.JPAApi;
import play.test.Helpers;
import play.test.WithApplication;

import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

public class ReservationDAOTest extends WithApplication {

    @Override
    public Application provideApplication() {
        return Helpers.fakeApplication(Helpers.inMemoryDatabase());
    }

    JPAApi jpaApi;
    ReservationDAO reservationDAO;

    @Before
    public void init() {
        // must get instances like this, as application is created for each test
        jpaApi = app.injector().instanceOf(JPAApi.class);
        reservationDAO = app.injector().instanceOf(ReservationDAO.class);

        // no need to setup / clear DB, WithApplication create new DB for each test
    }

    private void performNativeQuery(final String query) {
        jpaApi.withTransaction(em -> em.createNativeQuery(query).executeUpdate());
    }


    @Test
    public void testPersistReservation() {
        performNativeQuery("DELETE FROM reservation");

        final Account account = new Account();
        account.setUuid("16omrbidpprlb1iju8r1btbmb0");
        final Reservation reservation = new Reservation();
        reservation.setSum(5);
        reservation.setCreateDate(new Date());
        reservation.setAccount(account);

        final Reservation[] reservations = new Reservation[1];
        jpaApi.withTransaction(() -> {
            reservations[0] = reservationDAO.persistReservation(reservation);
        });
        // check returned reservation
        assertNotNull(reservations[0]);
        assertNotNull(reservations[0].getUuid()); // uuid was generated
        assertTrue(new ReflectionEquals(reservation, "uuid").matches(reservations[0]));

        // check stored reservation
        final Reservation storedReservation = jpaApi.withTransaction(em -> (Reservation) em.createQuery("SELECT res FROM Reservation res").getSingleResult());
        assertTrue(new ReflectionEquals(reservations[0], "account").matches(storedReservation));
        assertEquals(account.getUuid(), storedReservation.getAccount().getUuid());
    }

    @Test
    public void testDeleteReservation() {
        performNativeQuery("DELETE FROM reservation");
        performNativeQuery("INSERT INTO reservation (uuid, account_uuid, sum, create_date) VALUES ('uuid1', '16omrbidpprlb1iju8r1btbmb0', 5, CURRENT_TIMESTAMP)");


        jpaApi.withTransaction(() -> {
            final Reservation reservation = jpaApi.em().find(Reservation.class, "uuid1");
            reservationDAO.deleteReservation(reservation);
        });

        // check that no reservation is in DB reservation
        final List<Reservation> storedReservation = jpaApi.withTransaction(em -> em.createQuery("SELECT res FROM Reservation res").getResultList());
        assertTrue(storedReservation.isEmpty());
    }


}
