package data.dao;

import data.models.Account;
import exceptions.WrongParamException;
import org.junit.Before;
import org.junit.Test;
import org.mockito.internal.matchers.apachecommons.ReflectionEquals;
import play.Application;
import play.db.jpa.JPAApi;
import play.test.Helpers;
import play.test.WithApplication;

import java.util.List;

import static org.junit.Assert.*;

public class AccountDAOTest extends WithApplication {

    @Override
    public Application provideApplication() {
        return Helpers.fakeApplication(Helpers.inMemoryDatabase());
    }

    JPAApi jpaApi;
    AccountDAO accountDAO;

    @Before
    public void init() {
        // must get instances like this, as application is created for each test
        jpaApi = app.injector().instanceOf(JPAApi.class);
        accountDAO = app.injector().instanceOf(AccountDAO.class);

        // no need to setup / clear DB, WithApplication create new DB for each test
    }

    private void performNativeQuery(final String query) {
        jpaApi.withTransaction(em -> em.createNativeQuery(query).executeUpdate());
    }


    @Test
    public void testPersistAccount() {
        performNativeQuery("DELETE FROM account");

        final Account accountEntity = new Account();
        accountEntity.setCurrency("EUR");
        accountEntity.setBalance(5);
        accountEntity.setDebitEnabled(true);

        final Account[] account = new Account[1];
        jpaApi.withTransaction(() -> {
            account[0] = accountDAO.persistAccount(accountEntity);
        });
        // check returned account
        assertNotNull(account[0]);
        assertNotNull(account[0].getUuid()); // uuid was generated
        assertTrue(new ReflectionEquals(accountEntity, "uuid").matches(account[0]));

        // check stored user
        final Account storedAccount = jpaApi.withTransaction(em -> (Account) em.createQuery("SELECT acc FROM Account acc").getSingleResult());
        assertTrue(new ReflectionEquals(account[0], "reservations").matches(storedAccount));
    }

    @Test
    public void testGetAllAccounts_noAccounts() {
        performNativeQuery("DELETE FROM account");
        jpaApi.withTransaction(() -> {
            final List<String> result = accountDAO.getAllAccounts();

            assertTrue(result.size() == 0);
        });
    }

    @Test
    public void testGetAllUsers() {
        performNativeQuery("INSERT INTO account (balance, currency, debit_enabled, uuid) VALUES (1000, 'EUR', FALSE, 'uuid');");

        jpaApi.withTransaction(() -> {
            final List<String> result = accountDAO.getAllAccounts();

            assertTrue(result.size() == 3); // 2 from evolutions, one from test
        });
    }

    @Test
    public void testGetAccount() {
        performNativeQuery("DELETE FROM account");
        final String uuid = "uuid1";
        final Account entity = new Account();
        entity.setUuid(uuid);
        entity.setBalance(1000);
        entity.setDebitEnabled(false);
        entity.setCurrency("EUR");

        performNativeQuery("INSERT INTO account (balance, currency, debit_enabled, uuid) VALUES (1000, 'EUR', FALSE, '" + uuid + "');");

        jpaApi.withTransaction(() -> {
            final Account result = accountDAO.getAccount(uuid);

            assertNotNull(result);
            assertTrue(new ReflectionEquals(entity).matches(result));
        });
    }

    @Test
    public void testGetAccount_accountNotPresent() {
        final String uuid = "uuid1";

        performNativeQuery("INSERT INTO account (balance, currency, debit_enabled, uuid) VALUES (1000, 'EUR', FALSE, '" + uuid + "');");

        jpaApi.withTransaction(() -> {
            try {
                accountDAO.getAccount("invalid_uuid");
                fail();
            } catch (final WrongParamException e) {

            }
        });
    }

    @Test
    public void testGetAccount_nullParam() {
        final String uuid = "uuid1";

        performNativeQuery("INSERT INTO account (balance, currency, debit_enabled, uuid) VALUES (1000, 'EUR', FALSE, '" + uuid + "');");

        jpaApi.withTransaction(() -> {
            try {
                accountDAO.getAccount(null);
                fail();
            } catch (final WrongParamException e) {

            }
        });
    }
}
