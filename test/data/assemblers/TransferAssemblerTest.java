package data.assemblers;

import data.enums.TransferStatus;
import data.models.Account;
import data.models.Reservation;
import data.models.TransferRequest;
import data.pojos.TransferPojo;
import exceptions.WrongParamException;
import org.junit.Before;
import org.junit.Test;
import org.mockito.internal.matchers.apachecommons.ReflectionEquals;

import java.util.Date;

import static org.junit.Assert.assertTrue;

public class TransferAssemblerTest {

    private final TransferRequestAssembler transferRequestAssembler = new TransferRequestAssembler();

    private TransferPojo transferPojo;
    private TransferRequest transferRequest;

    @Before
    public void init() {
        final Date createDate = new Date();
        transferPojo = new TransferPojo();
        transferPojo.setReservation("reservation_uuid");
        transferPojo.setStatus(TransferStatus.OPEN);
        transferPojo.setCreateDate(createDate.getTime());
        transferPojo.setCurrency("EUR");
        transferPojo.setSum(5);
        transferPojo.setAccountFrom("account_from_uuid");
        transferPojo.setAccountTo("account_to_uuid");
        transferPojo.setRetryCount(1);

        final Reservation reservation = new Reservation();
        reservation.setUuid("reservation_uuid");

        final Account accountFrom = new Account();
        accountFrom.setUuid("account_from_uuid");
        final Account accountTo = new Account();
        accountTo.setUuid("account_to_uuid");

        transferRequest = new TransferRequest();
        transferRequest.setUuid("uuid1");
        transferRequest.setReservation(reservation);
        transferRequest.setStatus(TransferStatus.OPEN);
        transferRequest.setCreateDate(createDate);
        transferRequest.setCurrency("EUR");
        transferRequest.setSum(5);
        transferRequest.setAccountFrom(accountFrom);
        transferRequest.setAccountTo(accountTo);
        transferRequest.setRetryCount(1);
    }


    @Test
    public void testAssemblyPojo() {
        final TransferPojo result = transferRequestAssembler.assemblyPojo(transferRequest);
        assertTrue(new ReflectionEquals(transferPojo).matches(result));
    }

    @Test
    public void testAssemblyPojo_filledProcessDate() {
        final Date processDate = new Date();
        transferPojo.setProcessDate(processDate.getTime());
        transferRequest.setProcessDate(processDate);
        final TransferPojo result = transferRequestAssembler.assemblyPojo(transferRequest);
        assertTrue(new ReflectionEquals(transferPojo).matches(result));
    }

    @Test
    public void testAssemblyPojo_noReservation() {
        transferPojo.setReservation(null);
        transferRequest.setReservation(null);
        final TransferPojo result = transferRequestAssembler.assemblyPojo(transferRequest);
        assertTrue(new ReflectionEquals(transferPojo).matches(result));
    }

    @Test(expected = WrongParamException.class)
    public void testAssemblyPojo_nullParam() {
        transferRequestAssembler.assemblyPojo(null);
    }
}
