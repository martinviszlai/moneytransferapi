package data.assemblers;

import data.models.Account;
import data.models.Reservation;
import data.pojos.AccountPojo;
import exceptions.WrongParamException;
import org.junit.Before;
import org.junit.Test;
import org.mockito.internal.matchers.apachecommons.ReflectionEquals;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.Assert.assertTrue;

public class AccountAssemblerTest {

    private final AccountAssembler accountAssembler = new AccountAssembler();

    private AccountPojo accountPojo;
    private Account accountEntity;

    @Before
    public void init() {
        accountPojo = new AccountPojo();
        accountPojo.setUuid("uuid1");
        accountPojo.setCurrency("EUR");
        accountPojo.setBalance(5);
        accountPojo.setDebitEnabled(true);


        final Reservation reservation = new Reservation();
        reservation.setSum(1);

        accountEntity = new Account();
        accountEntity.setUuid("uuid1");
        accountEntity.setCurrency("EUR");
        accountEntity.setBalance(5);
        accountEntity.setDebitEnabled(true);
        accountEntity.setReservations(Arrays.asList(reservation));
    }

    @Test
    public void testAssemblyEntity() {
        final Account result = accountAssembler.assemblyEntity(accountPojo);
        assertTrue(new ReflectionEquals(accountEntity, "reservations").matches(result));
    }

    @Test(expected = WrongParamException.class)
    public void testAssemblyEntity_nullParam() {
        accountAssembler.assemblyEntity(null);
    }

    @Test
    public void testAssemblyPojo() {
        accountPojo.setDisposableBalance(4);
        final AccountPojo result = accountAssembler.assemblyPojo(accountEntity);
        assertTrue(new ReflectionEquals(accountPojo).matches(result));
    }

    @Test
    public void testAssemblyPojo_noReservations() {
        accountEntity.setReservations(new ArrayList<>());
        accountPojo.setDisposableBalance(5);
        final AccountPojo result = accountAssembler.assemblyPojo(accountEntity);
        assertTrue(new ReflectionEquals(accountPojo).matches(result));
    }

    @Test(expected = WrongParamException.class)
    public void testAssemblyPojo_nullParam() {
        accountAssembler.assemblyPojo(null);
    }
}
