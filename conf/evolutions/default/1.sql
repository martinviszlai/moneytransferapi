# --- !Ups
CREATE TABLE account (
  uuid          CHAR(36)   NOT NULL,
  balance       NUMBER     NOT NULL,
  currency      VARCHAR(3) NOT NULL,
  debit_enabled BOOLEAN    NOT NULL DEFAULT 'false',
  PRIMARY KEY (uuid)
);

CREATE TABLE reservation (
  uuid         CHAR(36)  NOT NULL,
  sum          NUMBER    NOT NULL,
  account_uuid CHAR(36)  NOT NULL,
  create_date  TIMESTAMP NOT NULL,
  PRIMARY KEY (uuid)
);

CREATE TABLE transfer_request (
  uuid              CHAR(36)   NOT NULL,
  sum               NUMBER     NOT NULL,
  currency          VARCHAR(3) NOT NULL,
  account_from_uuid CHAR(36)   NOT NULL,
  account_to_uuid   CHAR(36)   NOT NULL,
  create_date       TIMESTAMP  NOT NULL,
  process_date      TIMESTAMP,
  retry_count       NUMBER,
  status            NUMBER     NOT NULL,
  reservation_uuid  CHAR(36),
  PRIMARY KEY (uuid)
);

ALTER TABLE reservation
  ADD FOREIGN KEY (account_uuid) REFERENCES account (uuid);
ALTER TABLE transfer_request
  ADD FOREIGN KEY (account_from_uuid) REFERENCES account (uuid);
ALTER TABLE transfer_request
  ADD FOREIGN KEY (account_to_uuid) REFERENCES account (uuid);
ALTER TABLE transfer_request
  ADD FOREIGN KEY (reservation_uuid) REFERENCES reservation (uuid);


INSERT INTO account (balance, currency, debit_enabled, uuid) VALUES (1000, 'EUR', FALSE, '16omrbidpprlb1iju8r1btbmb0');
INSERT INTO account (balance, currency, debit_enabled, uuid) VALUES (1000, 'USD', FALSE, '18t9w9mx2t24e1ii3hsyltaw4v');