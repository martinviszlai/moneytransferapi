name := """MoneyTransferAPI"""
organization := "com.martinviszlai"

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava)

scalaVersion := "2.12.2"


libraryDependencies += guice
libraryDependencies += javaJpa
libraryDependencies += evolutions
libraryDependencies += "com.h2database" % "h2" % "1.4.194"
libraryDependencies += "org.hibernate" % "hibernate-core" % "5.2.5.Final"
//testing
libraryDependencies += "org.mockito" % "mockito-core" % "2.1.0" % "test"

// allow JPA in dist version
PlayKeys.externalizeResources := false
